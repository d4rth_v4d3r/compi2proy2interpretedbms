import java.awt.Desktop;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.charset.Charset;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Vector;

import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.UIManager;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.table.DefaultTableModel;

import julian.utils.gui.EditorCodigo;
import julian.utils.gui.EditorCodigo.StatusFlag;
import julian.utils.gui.EditorGUI;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;

public class SQLTest {

	private JFrame frame;
	public static Connection dbConn;
	public static Statement s;
	private static SQLResults pane;
	private EditorGUI tabbs;
	private JMenuBar menuBar;
	private static StringBuffer leErrs, siErrs, seErrs;
	private static boolean ok;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					try {
						UIManager
								.setLookAndFeel("com.sun.java.swing.plaf.nimbus.NimbusLookAndFeel");
					} catch (Exception e) {// Manejo de excepción...
						try {
							UIManager
									.setLookAndFeel("javax.swing.plaf.nimbus.NimbusLookAndFeel");
						} catch (Exception e1) {// Manejo de excepción...
							e.printStackTrace();
						}
					}
					setUIFont(new javax.swing.plaf.FontUIResource("Consolas",
							Font.PLAIN, 12));
					SQLTest window = new SQLTest();
					window.frame.setVisible(true);
					window.frame.pack();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	public static void setUIFont(javax.swing.plaf.FontUIResource f) {
		@SuppressWarnings("rawtypes")
		java.util.Enumeration keys = UIManager.getDefaults().keys();
		while (keys.hasMoreElements()) {
			Object key = keys.nextElement();
			Object value = UIManager.get(key);
			if (value != null
					&& value instanceof javax.swing.plaf.FontUIResource)
				UIManager.put(key, f);
		}
	}

	/**
	 * Create the application.
	 */
	public SQLTest() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		this.frame = new JFrame();
		this.frame.setIconImage(Toolkit.getDefaultToolkit().getImage(
				SQLTest.class.getResource("/resources/icon.png")));
		this.frame.setBounds(100, 100, 450, 300);
		this.frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.frame.setTitle("No SQL Workbench");
		this.menuBar = new JMenuBar();
		this.frame.setJMenuBar(this.menuBar);
		pane = new SQLResults();
		leErrs = new StringBuffer();
		siErrs = new StringBuffer();
		seErrs = new StringBuffer();
		tabbs = new EditorGUI();
		this.frame.setContentPane(tabbs);
		tabbs.agregarComponente("DB Output", pane, -1, EditorGUI.SALIDAS, true);
		cargarMenus();
		ok = true;
	}

	public static void execute(EditorCodigo editor) {
		try {
			pane.reset();
			Class.forName("com.mysql.jdbc.Driver");
			ok = true;
			leErrs.setLength(0);
			seErrs.setLength(0);
			siErrs.setLength(0);
			dbConn = DriverManager
					.getConnection("jdbc:mysql://localhost/?user=root&password=");
			s = dbConn.createStatement();

			try {
				SQLParser p = new SQLParser(editor.getF().getName(),
						new FileInputStream(editor.getF()));
				p.parse();
			} catch (Exception e1) {
				// TODO Auto-generated catch block
				editor.actualizarEstado("Termino la ejecucion con errores.",
						StatusFlag.ERROR);
			}
			s.close();
			dbConn.close();
		} catch (SQLException e) {
			editor.actualizarEstado("No se pudo abrir la conexion :(",
					StatusFlag.ERROR);
			return;
		} catch (ClassNotFoundException e) {
			editor.actualizarEstado("No se encontro el driver de MySQL:(",
					StatusFlag.ERROR);
			return;
		}
		pane.updateUI();

		if (ok)
			editor.actualizarEstado("Archivo ejecutado con exito :)",
					StatusFlag.OK);
		else
			editor.actualizarEstado(
					"El archivo termino de ejecutarse con errores",
					StatusFlag.ERROR);

		try {
			String html = IOUtils.toString(SQLTest.class.getClassLoader()
					.getResourceAsStream("resources/html.txt"), Charset
					.defaultCharset());
			FileUtils.write(new File(editor.getF().getAbsolutePath()
					+ ".lex.html"), String.format(html, "Errores Léxicos",
					"Errores Léxicos", leErrs.toString()), Charset
					.defaultCharset());
			FileUtils.write(new File(editor.getF().getAbsolutePath()
					+ ".sin.html"), String.format(html, "Errores Sintácticos",
					"Errores Sintácticos", siErrs.toString()), Charset
					.defaultCharset());
			FileUtils.write(new File(editor.getF().getAbsolutePath()
					+ ".sem.html"), String.format(html, "Errores Semánticos",
					"Errores Semánticos", seErrs.toString()), Charset
					.defaultCharset());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			editor.actualizarEstado(
					"No se pudieron generar los archivos de error :(",
					StatusFlag.ERROR);
		}
		System.gc();
	}

	public static void reportarErrorLexico(String archivo, int linea,
			int columna, String mensaje) {
		ok = false;
		leErrs.append(String
				.format("<tr><td> %s </td><td> %d </td><td> %d </td><td> %s. </td></tr>%s",
						archivo, linea, columna, mensaje,
						System.getProperty("line.separator")));
	}

	public static void reportarErrorSintactico(String archivo, int linea,
			int columna, String mensaje) {
		ok = false;
		siErrs.append(String
				.format("<tr><td> %s </td><td> %d </td><td> %d </td><td> %s. </td></tr>%s",
						archivo, linea, columna, mensaje,
						System.getProperty("line.separator")));
	}

	public static void reportarErrorSemantico(String archivo, int linea,
			int columna, String mensaje) {
		ok = false;
		seErrs.append(String
				.format("<tr><td> %s </td><td> %d </td><td> %d </td><td> %s. </td></tr>%s",
						archivo, linea, columna, mensaje,
						System.getProperty("line.separator")));
	}

	public static void graficarSQL(String results) {
		JTextArea jl = new JTextArea(results);
		jl.setEditable(false);
		jl.setEnabled(false);
		jl.setLineWrap(true);
		jl.setMaximumSize(new Dimension(600, 50));
		jl.setFont(new Font("CONSOLAS", Font.PLAIN, 12));
		pane.add(jl);
	}

	public static void graficarResultado(int results) {
		JTextArea jl = new JTextArea("---> " + results);
		jl.setEditable(false);
		jl.setMaximumSize(new Dimension(600, 30));
		jl.setFont(new Font("CONSOLAS", Font.PLAIN, 12));
		pane.add(jl);
	}

	public static void graficarResultado(ResultSet results) {
		try {
			JTable table = new JTable(construirTabla(results));
			JScrollPane jp = new JScrollPane(table);
			jp.setMaximumSize(new Dimension(600, 100));
			pane.add(jp);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			System.out.println("------->");
			e.printStackTrace();
		}

	}

	public static DefaultTableModel construirTabla(ResultSet rs)
			throws SQLException {

		ResultSetMetaData metaData = rs.getMetaData();

		Vector<String> aliasColumnas = new Vector<String>();
		int totalColumnas = metaData.getColumnCount();
		for (int column = 1; column <= totalColumnas; column++) {
			aliasColumnas.add(metaData.getColumnName(column));
		}

		Vector<Vector<Object>> tabla = new Vector<Vector<Object>>();
		while (rs.next()) {
			Vector<Object> tupla = new Vector<Object>();
			for (int indice = 1; indice <= totalColumnas; indice++) {
				tupla.add(rs.getObject(indice));
			}
			tabla.add(tupla);
		}

		return new DefaultTableModel(tabla, aliasColumnas);

	}

	private void cargarMenus() {
		String[] items_archivo = new String[] { "Abrir un archivo...",
				"Actualizar archivo...", "Salir" };
		ActionListener[] listeners_archivo = new ActionListener[] {
				new ActionListener() {

					@Override
					public void actionPerformed(ActionEvent e) {
						// TODO Auto-generated method stub
						EditorCodigo codigo = new EditorCodigo();
						File f = abrirArchivo("Archivos de codigo no sql.",
								"db", codigo);
						if (f != null) {
							codigo.cargarArchivo(f);
							codigo.editable(true);
							tabbs.agregarComponente(f.getName(), codigo, -1,
									EditorGUI.ENTRADAS, false);
						}
					}
				}, new ActionListener() {

					@Override
					public void actionPerformed(ActionEvent e) {
						// TODO Auto-generated method stub
						EditorCodigo editor = (EditorCodigo) (EditorCodigo) tabbs
								.componenteActual(EditorGUI.ENTRADAS);
						if (editor != null)
							editor.refrescarArchivo();
					}
				}, new ActionListener() {

					@Override
					public void actionPerformed(ActionEvent e) {
						// TODO Auto-generated method stub
						System.exit(0);
					}
				} };
		agregarMenu("Archivo", items_archivo, listeners_archivo);
		String[] items_mostrar = new String[] { "Errores Léxicos",
				"Errores Sintácticos", "Errores Semánticos" };

		ActionListener[] listeners_mostrar = new ActionListener[] {
				new ActionListener() {

					@Override
					public void actionPerformed(ActionEvent e) {
						// TODO Auto-generated method stub
						if (!ok) {
							EditorCodigo editor = (EditorCodigo) (EditorCodigo) tabbs
									.componenteActual(EditorGUI.ENTRADAS);
							if (editor != null)
								try {
									Desktop.getDesktop().open(
											new File(editor.getF()
													.getAbsolutePath()
													+ ".lex.html"));
								} catch (IOException e1) {
									// TODO Auto-generated catch block
									editor.actualizarEstado(
											"No se pudo abrir el archivo de errores léxicos.",
											StatusFlag.ERROR);
								}
						}
					}
				}, new ActionListener() {

					@Override
					public void actionPerformed(ActionEvent e) {
						// TODO Auto-generated method stub
						if (!ok) {
							EditorCodigo editor = (EditorCodigo) (EditorCodigo) tabbs
									.componenteActual(EditorGUI.ENTRADAS);
							if (editor != null)
								try {
									Desktop.getDesktop().open(
											new File(editor.getF()
													.getAbsolutePath()
													+ ".sin.html"));
								} catch (IOException e1) {
									// TODO Auto-generated catch block
									editor.actualizarEstado(
											"No se pudo abrir el archivo de errores sintácticos.",
											StatusFlag.ERROR);
								}
						}
					}
				}, new ActionListener() {

					@Override
					public void actionPerformed(ActionEvent e) {
						// TODO Auto-generated method stub
						if (!ok) {
							EditorCodigo editor = (EditorCodigo) (EditorCodigo) tabbs
									.componenteActual(EditorGUI.ENTRADAS);
							if (editor != null)
								try {
									Desktop.getDesktop().open(
											new File(editor.getF()
													.getAbsolutePath()
													+ ".sem.html"));
								} catch (IOException e1) {
									// TODO Auto-generated catch block
									editor.actualizarEstado(
											"No se pudo abrir el archivo de errores semánticos.",
											StatusFlag.ERROR);
								}
						}
					}
				} };
		agregarMenu("Mostrar", items_mostrar, listeners_mostrar);

		String[] items_ejecutar = new String[] { "Ejecutar" };

		ActionListener[] listeners_ejecutar = new ActionListener[] { new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub

				EditorCodigo codigo = (EditorCodigo) (EditorCodigo) tabbs
						.componenteActual(EditorGUI.ENTRADAS);
				if (codigo != null)
					if (codigo.getF() != null)
						execute(codigo);
			}
		}, };
		agregarMenu("Ejecutar", items_ejecutar, listeners_ejecutar);
	}

	/**
	 * Agrega un menu en la barra de herramientas.
	 * 
	 * @param nombre
	 *            El nombre del menu.
	 * @param items
	 *            La lista de items del menu.
	 * @param listeners
	 *            Las acciones que ejecutará cada item.
	 */
	public void agregarMenu(String nombre, String[] items,
			ActionListener[] listeners) {
		if (items.length != listeners.length) {
			System.err
					.println("El tamaño de la lista de items y listeners debe ser el mismo");
			return;
		}
		JMenu jm = new JMenu(nombre);
		int index = 0;
		for (String s : items) {
			JMenuItem jmi = new JMenuItem(s);
			jmi.addActionListener(listeners[index]);
			++index;
			jm.add(jmi);
		}

		this.menuBar.add(jm);
	}

	/**
	 * Abre un archivo en un editor.
	 * 
	 * @param desc_filtro
	 *            La descripcion del filtro
	 * @param filtro
	 *            La extension del filtro
	 * @param editor
	 *            El editor donde se va a cargar
	 * @return El archivo cargado
	 */
	public File abrirArchivo(String desc_filtro, String filtro,
			EditorCodigo editor) {
		FileNameExtensionFilter filter = new FileNameExtensionFilter(
				desc_filtro, filtro);
		final JFileChooser fc = new JFileChooser();
		fc.setFileFilter(filter);
		int returnVal = fc.showOpenDialog(this.frame);
		if (returnVal == JFileChooser.APPROVE_OPTION)
			return fc.getSelectedFile();
		else if (returnVal == JFileChooser.CANCEL_OPTION)
			return null;
		else if (returnVal == JFileChooser.ERROR_OPTION)
			editor.actualizarEstado("Error al abrir el archivo.",
					StatusFlag.ERROR);
		else
			editor.actualizarEstado("Error desconocido.", StatusFlag.ERROR);
		return null;
	}

}
