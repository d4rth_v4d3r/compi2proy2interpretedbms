import java.awt.BorderLayout;
import java.awt.Component;

import javax.swing.BoxLayout;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

public class SQLResults extends JPanel {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1839962826683212811L;
	private JScrollPane scrollPane;
	private JPanel container;

	/**
	 * Create the panel.
	 */
	public SQLResults() {
		setLayout(new BorderLayout(0, 0));

		this.scrollPane = new JScrollPane();
		add(this.scrollPane, BorderLayout.CENTER);

		this.container = new JPanel();
		this.scrollPane.setViewportView(this.container);
		this.container
				.setLayout(new BoxLayout(this.container, BoxLayout.Y_AXIS));
	}

	@Override
	public Component add(Component comp) {
		// TODO Auto-generated method stub
		return container.add(comp);
	}

	public void reset() {
		this.container.removeAll();
	}
}
