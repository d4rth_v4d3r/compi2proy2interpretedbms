import java_cup.runtime.*;

/**
 * @author Julian Chamale <jchamale.usac@gmail.com>
 * 
 */
%%
%{
	private String archivo;
	SQLScanner(java.io.InputStream is, String archivo) {
		this(is);
		this.archivo = archivo;
	}

	private Symbol sym(int type) {
		return sym(type, yytext());
	}
	
	private Symbol sym(int type, Object value) {
		return new Symbol(type, yyline + 1, yycolumn + 1, value);
	}

%}

%init{
		
%init}

%eofval{	
		return sym(SQLSyms.EOF);
%eofval}

%class SQLScanner
%public
%cup
%unicode
%ignorecase
%line  	//definimos el uso de contador de lineas
%char  	//definimos el uso de contador de caracteres
%column

character	= 	[A-Za-z]
digit		=	[0-9]
cadena		=	\"(\\\"|[^\"])*\"
entero		=	{digit}+
decimal		= 	{entero}"."{entero}
id 			=	{character}({character}|{digit}|"_")*
fecha		=	{digit}{digit}"/"{digit}{digit}"/"{digit}{digit}{digit}{digit}

%%



	//=================================== INICIO KEYWORDS  =======================================

	"usar"					{ return sym(SQLSyms.USAR); 	}
	"nombre"				{ return sym(SQLSyms.NOMBRE); 	}
	"creartabla"			{ return sym(SQLSyms.CREARTABLA); 	}
	"actualizar"			{ return sym(SQLSyms.ACTUALIZAR); 	}
	"campo"					{ return sym(SQLSyms.CAMPO); 	}
	"campos"				{ return sym(SQLSyms.CAMPOS); 	}
	"tipo"					{ return sym(SQLSyms.TIPO); 	}
	"insercion"				{ return sym(SQLSyms.INSERCION); 	}
	"tabla"					{ return sym(SQLSyms.TABLA); 	}
	"consultar"				{ return sym(SQLSyms.CONSULTAR); 	}
	"condicion"				{ return sym(SQLSyms.CONDICION); 	}
	"funcion"				{ return sym(SQLSyms.FUNCION); 	}
	"contar"				{ return sym(SQLSyms.CONTAR); 	}
	"sumar"					{ return sym(SQLSyms.SUMAR); 	}
	"promedio"				{ return sym(SQLSyms.PROMEDIO); 	}
	"atributo"				{ return sym(SQLSyms.ATRIBUTO); 	}
	"eliminar"				{ return sym(SQLSyms.ELIMINAR); 	}
	"valor"					{ return sym(SQLSyms.VALOR); 	}
	
	"{"						{ return sym(SQLSyms.LBRACE); 	}
	"}"						{ return sym(SQLSyms.RBRACE); 	}
	"<"						{ return sym(SQLSyms.OPREL); 	}
	"<="					{ return sym(SQLSyms.OPREL); 	}
	">"						{ return sym(SQLSyms.OPREL); 	}
	">="					{ return sym(SQLSyms.OPREL); 	}
	"=="					{ return sym(SQLSyms.OPREL, "="); 	}
	"!="					{ return sym(SQLSyms.OPREL, "<>"); 	}
	"and"					{ return sym(SQLSyms.AND); 	}
	"or"					{ return sym(SQLSyms.OR); 	}
	"not"					{ return sym(SQLSyms.NOT); 	}
	":"						{ return sym(SQLSyms.COLON); 	}
	
	//=================================== INICIO DE VALORES  =======================================
	"entero"				{ return sym(SQLSyms.ENTERO); 	}
	"decimal"				{ return sym(SQLSyms.DECIMAL); 	}
	"fecha"					{ return sym(SQLSyms.FECHA); 	}
	"cadena"				{ return sym(SQLSyms.CADENA); 	}
	{fecha}					{ return sym(SQLSyms.VALFECHA); 	}
	{entero}				{ return sym(SQLSyms.VALENTERO); 	}
	{decimal}				{ return sym(SQLSyms.VALDECIMAL); 	}
	{cadena}				{ return sym(SQLSyms.VALCADENA); }
	{id}					{ return sym(SQLSyms.ID); }
	[" "\t]+				{ }	
	[\n\r\f]+				{ }
	
	//=================================== INICIO ESPACIOS EN BLANCO, COMENTARIOS Y ERRORES  =======================================	
	.						{ SQLTest.reportarErrorLexico(
								this.archivo, yyline + 1, yycolumn, "Token '" + yytext() + "' no reconocido."); }
